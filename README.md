# ArmA3 Factions (working title) #

ArmA3 Factions is a Free-For-All Sandbox RPG Muliplayer Game Mode

### To Do ###


* Build spawn system
* Create Spawn Zones
* Build player save BETA


## Abstract ##

* All players will be side:civilian. Features will include
* Gather/Processing type initiatives
* Missions with hostile AI
* PvP and TvT engagements (free-for-all)
* Persistent Leveling System
* Skill Trees to unlock gear, etc

### Class System ###
* Specializations for skill trees - current path is like: 1) Infantry 2) Marksman 3) SOF 4) Support
* Infantry would get basic assault rifles and access to RCO-style optics
* Marksman would get DMS / SOS / LRPS style optics as unlocks as well as DMR rifles
* SOF would get suppressors for basic assualt rifles and DMR rifles
* Support would gain access to LMGs, etc
* Each class type should receive a special that will unlock at high level
* Infantry might unlock a static weapon (LMG)
* Marksman might unlock a laser guided bomb that they can call in 
* SOF might unlock a HALO skill that drops them from high altitude at a desired area
* Support could call in artillery strikes
* Each 'Special' skill that a class unlocks would be a one-time purchase (non-persistent) that is a high-dollar investment
* Rough pricing schedules: Static LMG - $50,000; Laser guided Bomb - $65,000; HALO Jump - $55,000; Artillery Strike - $80,000;

### Groups, Activities, Vendors ###
* Persistent Groups will be enabled
* Vehicle locking and garage system should be allowed
* There will be pre-built bases on-map for purchase that should be upgradable
* Base upgrades may include expansions, vendors, garages, etc

### Leveling System ###
* Completing Objectives and killing other players, gathering and processing, selling will all grant XP
* Selling a processed item will award 5xp / unit
* Killing another player will award 100xp
* Completing an objective will award the player / group with 500xp
* Basic XP / Level is based on a logrithmic formula: 

* _constA = 8.7;
* _constB = -40;
* _constC = 111;
* _level = Math.max( Math.floor( _constA * Math.log( XP + _constC ) + _constB ), 1 );

####NOTES####
* If initial levels should be higher, increase ConstA and decrease ConstB and could set ConstC to something like exp((1-constB)/constA)
* Also, this forumla is based on saving a  player's TOTAL XP and then calculating their level, then we only have to save totalXP
* To get lower XP of current level feed level like so: (Math.exp((level - _constB) / _constA) - _constC)
* To get max XP of current level feed level+1 like so: (Math.exp(((level+1) - _constB) / _constA) - _constC)