waitUntil {!isNull player && player == player};
if(player diarySubjectExists "rules")exitwith{};


player createDiarySubject ["factions","Arma 3 Factions"];

player createDiaryRecord["factions",
	[
		"Welcome",
		"
		Welcome to Arma3 Factions<br/><br/>
		Arma3 Factions is a Sandbox RPG Free-For-All game-mode currently in development.
		"
	]
];