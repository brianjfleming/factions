diag_log "";
diag_log "";
diag_log "";
diag_log "------------------------------------------------------------------------------------------------------";
diag_log "--------------------------------    Starting Factions Client Init    ---------------------------------";
diag_log "------------------------------------------------------------------------------------------------------";
diag_log "";
diag_log "";
diag_log "";


if(!hasInterface) exitWith {}; // Headless Client

/* ************************************* */
/* Set Up Variables (DEV)				 */
/* ************************************* */
player globalChat "-- Setting up player Variables";
player setVariable["steam64ID",getPlayerUID player];
player setVariable["realname",profileName,true];

/* ************************************* */
/* I say pfft to fatigue				 */
/* ************************************* */
player enableFatigue false;


[] execVM "factions\init.sqf";