class Factions_Client_Core
{
	tag = "factions";
	
	class Functions
	{
		file = "factions\functions";
		class hudSetup {};
		class hudUpdate {};
		class xpUpdate {};
		class handleDamage {};
		class playerKilled {};
		class setupEVH {};
	}
	
}