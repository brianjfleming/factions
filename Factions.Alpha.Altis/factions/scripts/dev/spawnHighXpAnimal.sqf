private["_playerPos","_playerName","_animals","_classname","_radius","_spwnPos","_animalgroup","_animal"];

_playerPos = _this select 0;
_radius = 100;
_spwnPos = [_playerPos,random _radius, random 360] call BIS_fnc_relPos;

_animals = ["Hen_random_F","Cock_random_F","Goat_random_F","Sheep_random_F"];
_classname = _animals call BIS_fnc_selectRandom;

_animalgroup = createGroup Civilian;
_animal = _animalgroup createUnit[_classname,_spwnPos,[],0,"NONE"];
_animal addEventHandler["Killed", {
	_unit = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
	_killer = [_this,1,ObjNull,[ObjNull]] call BIS_fnc_param;
	_uid = getPlayerUID _killer;
	_pname = name _killer;
	player globalChat format["Animal was killed by %1",_pname];
	_updateArr = [_uid,1000];
	[_updateArr, "factions_fnc_xpUpdate",nil,false] call BIS_fnc_MP
}];