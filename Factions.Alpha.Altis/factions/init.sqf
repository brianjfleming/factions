
// Dev Constants
survival_hunger = 100;
survival_thirst = 100;

factions_lvl = 1;
factions_lvlProgress = 0;
factions_lvlAmount = ((factions_lvl * 7500) / 2);

/* ************************************* */
/* Set Up Event Handlers				 */
/* ************************************* */
[] call factions_fnc_setupEVH;

/* ************************************* */
/* Set Up Hud							 */
/* ************************************* */
[] call factions_fnc_hudSetup;

/* ************************************* */
/* Survival and Dev Stuff				 */
/* ************************************* */
[] execVM "factions\survival\init_survival.sqf";
[] execVM "factions\scripts\earplugs\earplugInit.sqf";
[] execVM "factions\scripts\dev\devInit.sqf";
[] execVM "factions\scripts\welcome\init.sqf";

/* ************************************* */
/* Spawn Selection						 */
/* ************************************* */