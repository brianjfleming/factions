private["_pid","_addXP","_step","_uid"];
_pid = _this select 0;
_addXP = _this select 1;
_step = (_addXP/10);
_uid = getPlayerUID player;

// Add XP if you are the awarded player
if(_uid == _pid) then {
	// Only play if we aren't leveling up
	if((_addXP + factions_lvlProgress) <= factions_lvlAmount) then {
		player say3D "XP";
	};
	
	for "_i" from 1 to _step do {
		if(factions_lvlProgress == factions_lvlAmount) then {
			factions_lvl = factions_lvl + 1;
			factions_lvlProgress = 0;
			factions_lvlAmount = ((factions_lvl * 7500) / 2);
			
			player say3D "LevelUp";
		};
	
		factions_lvlProgress = factions_lvlProgress + 10;
		diag_log format["XP: %1",factions_lvlProgress];
		[] call factions_fnc_hudUpdate; // Update our HUD
		sleep 0.05;
	};
};