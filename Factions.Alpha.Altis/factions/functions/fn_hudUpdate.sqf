/*
	File: fn_hudUpdate.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Updates the HUD when it needs to.
*/
private["_ui","_food","_water","_health","_lvlBar","_lvlText"];
disableSerialization;

_ui = uiNameSpace getVariable ["playerHUD",displayNull];
if(isNull _ui) then {[] call factions_fnc_hudSetup;};
_food = _ui displayCtrl 23500;
_water = _ui displayCtrl 23510;
_health = _ui displayCtrl 23515;

_lvlBar = _ui displayCtrl 23520;
_lvlText = _ui displayCtrl 23525;

//Update food
_food ctrlSetPosition [safeZoneX+safeZoneW-0.17,safeZoneY+safeZoneH-0.05];
_food ctrlSetText format["%1", survival_hunger];
_food ctrlCommit 0;

//Update Water
_water ctrlSetPosition [safeZoneX+safeZoneW-0.27,safeZoneY+safeZoneH-0.05];
_water ctrlSetText format["%1", survival_thirst];
_water ctrlCommit 0;

//Update Health
_health ctrlSetPosition [safeZoneX+safeZoneW-0.07,safeZoneY+safeZoneH-0.05];
_health ctrlSetText format["%1", round((1 - (damage player)) * 100)];
_health ctrlCommit 0;

//Update Level Text
_lvlText ctrlSetText format["Level %1 - %2 / %3 XP",factions_lvl,factions_lvlProgress,factions_lvlAmount];
_lvlText ctrlCommit 0;

//Update Level Progress Bar
_lvlBar ctrlSetPosition [safeZoneX,safeZoneY+safeZoneH-0.06,safeZoneW * (factions_lvlProgress/factions_lvlAmount),0.06];
_lvlBar ctrlCommit 0;