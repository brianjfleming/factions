private["_unit","_damage","_source","_projectile","_part","_curWep"];
_unit = _this select 0;
_part = _this select 1;
_damage = _this select 2;
_source = _this select 3;
_projectile = _this select 4;

player globalChat format["PART: %1 || DAMAGE: %2 || SOURCE: %3 || PROJECTILE: %4",_part,_damage,_source,_projectile];

[] call factions_fnc_hudUpdate;
_damage;