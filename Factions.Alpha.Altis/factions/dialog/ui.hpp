	class playerHUD
   	{
		idd=-1;
		movingEnable=0;
	  	fadein=0;
		duration = 99999999999999999999999999999999999999999999;
	  	fadeout=0;
		name="playerHUD";
		onLoad="uiNamespace setVariable ['playerHUD',_this select 0]";
		objects[]={};
		
		class controlsBackground 
		{
	
			class HudBackground:Factions_RscText {
				idc = -1;
				colorBackground[] = {0, 0, 0, 0.6};
				x = safeZoneX; y = safeZoneY+safeZoneH-0.06;
				w = safeZoneW; h = 0.06;
			};
	
			class HudProgress:Factions_RscText {
				idc = 23520;
				colorBackground[] = {0, 0, 0, 1};
				x = safeZoneX; y = safeZoneY+safeZoneH-0.06;
				w = safeZoneW * .35; h = 0.06;
			};
	
			class HudProgressText:Factions_RscText {
				idc = 23525;
				colorBackground[] = {0, 0, 0, 0};
				text = "Level 1 - 35%";
				size = .04;
				font = "PuristaMedium";
				colorText[] = {1,1,1,1};
				x = safeZoneX+0.02; y = safeZoneY+safeZoneH-0.06;
				w = 0.50; h = 0.06;
			};
			
			class waterHIcon:Factions_RscPicture 
			{
			
				idc = -1;
				text = "icons\water_hud.paa";
				x = safeZoneX+safeZoneW-0.30; y = safeZoneY+safeZoneH-0.05;
				w = 0.03; h = 0.04;
			};
			
			class foodHIcon:Factions_RscPicture 
			{
			
				idc = -1;
				text = "icons\food_hud.paa";
				x = safeZoneX+safeZoneW-0.20; y = safeZoneY+safeZoneH-0.05;
				w = 0.03; h = 0.04;
			};
			
			class healthHIcon:Factions_RscPicture
			{
				
				idc = -1;
				text = "icons\health_hud.paa";
				x = safeZoneX+safeZoneW-0.10; y = safeZoneY+safeZoneH-0.05;
				w = 0.03; h = 0.04;
			};
		};
		
		class controls
		{
			
			class watertext
			{
				type=0;
				idc=23510;
				style=0;
				x=-1;
				y=-1;
				w=0.3;
				h=0.04;
				sizeEx=0.04;
				font="PuristaMedium";
				colorBackground[]={0,0,0,0};
				colorText[] = {1,1,1,1};
				shadow=true;
				text="";
			};
			
			class foodtext
			{
				type=0;
				idc=23500;
				style=0;
				x=-1;
				y=-1;
				w=0.3;
				h=0.04;
				sizeEx=0.04;
				font="PuristaMedium";
				colorBackground[]={0,0,0,0};
				colorText[] = {1,1,1,1};
				shadow=true;
				text="";
			};
			
			class healthtext
			{
				type=0;
				idc=23515;
				style=0;
				x=-1;
				y=-1;
				w=0.3;
				h=0.04;
				sizeEx=0.04;
				font="PuristaMedium";
				colorBackground[]={0,0,0,0};
				colorText[] = {1,1,1,1};
				shadow=true;
				text="";
			};
		};   
 	};